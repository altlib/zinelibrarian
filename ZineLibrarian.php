<?php
/**
 * Plugin Name: ZineLibrarian
 * Plugin URI:  https://altlib.org
 * Description: A zine catalog for WordPress
 * Version:     0.9.0
 * Author:      The Bellingham Alternative Library
 * Author URI:  https://altlib.org
 * License:     GPL v2 or later
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 */

/*
 * ZineLibrarian - A zine catalog for WordPress
 * Copyright (C) 2021 The Bellingham Alternative Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
require_once(plugin_dir_path(__FILE__)."services/models/DistroModel.php");
require_once(plugin_dir_path(__FILE__)."services/models/ZineModel.php");

require_once(plugin_dir_path(__FILE__)."services/DistroSvc.php");
require_once(plugin_dir_path(__FILE__)."services/PluginSvc.php");
require_once(plugin_dir_path(__FILE__)."services/ZineSvc.php");

if (!class_exists("ZineLibrarian")) {
    /**
     * A zine catalog for WordPress
     */
    class ZineLibrarian
    {
        /**
         * @var DistroSvc $_distroSvc
         */
        private $_distroSvc;

        /**
         * @var PluginSvc $_pluginSvc
         */
        private $_pluginSvc;

        /**
         * @var ZineSvc $_zineSvc
         */
        private $_zineSvc;

        /**
         * @var array $page_templates
         */
        private $page_templates;

        /**
         * A zine catalog for WordPress
         */
        function __construct()
        {
            register_activation_hook(__FILE__, array($this, "hook_activation"));
            register_deactivation_hook(__FILE__, array($this, "hook_deactivation"));
            
            add_action("init", array($this, "action_init"));
            add_action("wp_print_scripts", array($this, "action_print_scripts"));

            add_action("plugins_loaded", array($this, "action_plugins_loaded"));

            add_filter("theme_page_templates", array($this, "add_new_template"));
            add_filter("wp_insert_post_data", array($this, "insert_post_data"));
            add_filter("template_include", array($this, "template_include"));

            $this->page_templates = array(
                "templates/DistroInfo.php"  => "Distro Info",
                "templates/ZineInfo.php"    => "Zine Info"
            );

            add_action("admin_menu", array($this, "action_admin_menu"));

            add_action("wp_ajax_get_distro_by_id", array($this, "ajax_get_distro_by_id"));
            add_action("wp_ajax_is_distro_by_name", array($this, "ajax_is_distro_by_name"));
            add_action("wp_ajax_check_distroid_zine_count", array($this, "ajax_check_distroid_zine_count"));
            add_action("wp_ajax_check_distroname_zine_count", array($this, "ajax_check_distroname_zine_count"));
            add_action("wp_ajax_edit_distro", array($this, "ajax_edit_distro"));
            add_action("wp_ajax_delete_distro", array($this, "ajax_delete_distro"));
            add_action("wp_ajax_get_zine_by_id", array($this, "ajax_get_zine_by_id"));
            add_action("wp_ajax_is_zine_by_name", array($this, "ajax_is_zine_by_name"));
            add_action("wp_ajax_edit_zine", array($this, "ajax_edit_zine"));
            add_action("wp_ajax_delete_zine", array($this, "ajax_delete_zine"));

            $this->_distroSvc = new DistroSvc();
            $this->_pluginSvc = new PluginSvc();
            $this->_zineSvc = new ZineSvc();
        }

        /**
         * Actions to perform on activation in Plugin settings
         */
        function hook_activation(): void
        {
            $version = get_option("zl_version");

            switch($version) {
            case "0.9.0":
                break;
            default:
                if (!$version)
                    $this->first_install();
                break;
            }
        }

        /**
         * Actions to take on the first install of this plugin
         */
        private function first_install(): void
        {
            add_option("zl_version", "0.9.0");

            $role = get_role("administrator");
                $role->add_cap("manage_zines");
            $role = get_role("editor");
                $role->add_cap("manage_zines");

            $this->_pluginSvc->database_create();
        }

        /**
         * Actions to take when deactivating this plugin
         */
        function hook_deactivation(): void
        { }

        /**
         * Actions to take when initializing the plugin
         */
        function action_init(): void
        { 
            wp_enqueue_style("zinelib-bootstrap", plugin_dir_url(__FILE__)."css/bootstrap.min.css");
        }

        /**
         * Actions to take when ready to apply scripts
         */
        function action_print_scripts(): void
        {
            wp_enqueue_script("zinelib-bootstrap", plugin_dir_url(__FILE__)."js/bootstrap.bundle.min.js", array("jquery"));

            wp_enqueue_script("zinelib-front", plugin_dir_url(__FILE__)."js/front.min.js");
        }

        /**
         * Actions to take when all plugins are loaded
         */
        function action_plugins_loaded(): void
        { }

        /**
         * Adds local templates to the selector in the page editor
         */
        function add_new_template(array $page_templates): array
        {
            $page_templates = array_merge($page_templates, $this->page_templates);

            return $page_templates;
        }

        /**
         * Filters to apply to post data before committing to database
         */
        function insert_post_data(array $postarr): array
        {
            $this->insert_page_templates();
            
            return $postarr;
        }

        /**
         * Inserts the page templates to the post cache
         */
        private function insert_page_templates(): void
        {
            $cache_key = "page_templates-".md5(get_theme_root()."/".get_stylesheet());

            $page_templates = wp_get_theme()->get_page_templates();
            if (empty($page_templates)) {
                $page_templates = array();
            }
            $page_templates = array_merge($page_templates, $this->page_templates);

            wp_cache_delete($cache_key, "themes");
            wp_cache_add($cache_key, $page_templates, "themes", 1800);
        }

        /**
         * Determines the path for custom local templates
         */
        function template_include(string $template): string
        {
            global $post;

            if (!$post || !isset($this->page_templates[get_post_meta($post->ID, "_wp_page_template", true)]))
                return $template;

            $file = plugin_dir_path(__FILE__).get_post_meta($post->ID, "_wp_page_template", true);

            if (file_exists($file))
                return $file;

            return $template;
        }

        /**
         * Admin menu additions
         */
        function action_admin_menu(): void
        {
            add_menu_page(
                "ZineLibrarian",
                "ZineLibrarian",
                "manage_zines",
                "zinelib_admin_menu",
                array($this, "admin_menu_distro"),
                "dashicons-book-alt",
                null
            );
            add_submenu_page(
                "zinelib_admin_menu",
                "Manage Distros",
                "Manage Distros",
                "manage_zines",
                "zinelib_admin_menu",
                array($this, "admin_menu_distro"),
                null
            );
            add_submenu_page(
                "zinelib_admin_menu",
                "Manage Zines",
                "Manage Zines",
                "manage_zines",
                "zinelib_admin_zine",
                array($this, "admin_menu_zine"),
                null
            );
        }
    
        /**
         * Display the distro management screen
         */
        function admin_menu_distro(): void
        {
            include plugin_dir_path(__FILE__)."templates/admin/ManageDistros.php";
        }

        /**
         * Retrieve a distro by ID
         */
        function ajax_get_distro_by_id(): void
        {
            echo json_encode($this->_distroSvc->get($_GET["id"]));

            wp_die();
        }

        /**
         * Retrieve if a distro exists by name
         */
        function ajax_is_distro_by_name(): void
        {
            echo $this->_distroSvc->exists_by_name($_GET["name"]);

            wp_die();
        }
        
        /**
         * Check if a given distro ID is used by zines
         */
        function ajax_check_distroid_zine_count(): void
        {
            echo $this->_distroSvc->id_count($_GET["id"]);

            wp_die();
        }

        /**
         * Check if a given distro name is used by zines
         */
        function ajax_check_distroname_zine_count(): void
        {
            echo $this->_distroSvc->name_count($_GET["name"]);

            wp_die();
        }

        /**
         * Edit a distro async callback
         */
        function ajax_edit_distro(): void
        {
            $postModel = new DistroPostModel();
            $id = $_POST["id"];

            $postModel->Name = $_POST["name"];
            $postModel->Bio = $_POST["bio"];
            $postModel->Link = $_POST["link"];
            $postModel->Image = $_POST["image"];

            if ($id == "-1")
                echo $this->_distroSvc->create($postModel);
            else
                echo $this->_distroSvc->update($id, $postModel);

            wp_die();
        }

        /**
         * Delete a distro callback
         */
        function ajax_delete_distro(): void
        {
            $this->_distroSvc->delete($_REQUEST["id"]);

            wp_die();
        }
    
        /**
         * Display the zine management screen
         */
        function admin_menu_zine(): void
        {
            include plugin_dir_path(__FILE__)."templates/admin/ManageZines.php";
        }

        /**
         * Retrieve an individual zine
         */
        function ajax_get_zine_by_id(): void
        {
            echo json_encode($this->_zineSvc->get($_GET["id"]));

            wp_die();
        }

        /**
         * Retrieve if a zine exists by name
         */
        function ajax_is_zine_by_name(): void
        {
            echo $this->_zineSvc->exists_by_name($_GET["name"]);

            wp_die();
        }

        /**
         * Add a zine async callback
         */
        function ajax_edit_zine(): void
        {
            $postModel = new ZinePostModel();
            $id = $_POST["id"];

            $postModel->Name = $_POST["name"];
            $postModel->Description = $_POST["desc"];
            $postModel->DistroName = $_POST["distro"];
            $postModel->PubDate = $_POST["pubdate"];
            $postModel->Link = $_POST["link"];
            $postModel->Image = $_POST["image"];

            if ($id == "-1")
                echo $this->_zineSvc->create($postModel);
            else
                echo $this->_zineSvc->update($id, $postModel);

            wp_die();
        }

        /**
         * Delete a zine callback
         */
        function ajax_delete_zine(): void
        {
            $this->_zineSvc->delete($_REQUEST["id"]);

            wp_die();
        }
    }

    /**
     * The globally visible interface to the plugin
     */
    global $ZineLibrarian;
    $ZineLibrarian = new ZineLibrarian();
}
