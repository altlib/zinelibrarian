# ZineLibrarian

## Description
ZineLibrarian is a zine cataloging system for use by The Bellingham Alternative Library. The system should be generic enough to extend any WordPress site with an integrated zine library.

## Building
First, install the necessary packages with **npm install**. Then, issue **npm run build** to stage a build of the plugin in ./dev. This will be used by Docker or can be copied to a local WordPress site. Issue **npm run dist** to stage a build of the plugin in ./dist for deploying to a public website.

## Liability
This plugin does not seek to fit a particular purpose, and is intended for use on the Bellingham Alternative Library WordPress website. Feel free to modify the code and use it in your own projects, but note that the GPL disclaims any liability on our part. In short, this isn't built for you, so don't be surprised when it doesn't do precisely what you want.

## Using Docker
Docker compose will look for the following files in docker-files:

- 00-wordpress.sql - A SQL dump of an existing WordPress site for testing
- wp-content - Any additional wp-content needed

With these files in place, build zinelibrarian then issue **npm run compose**

You should then be able to access your site at http://localhost.

## Using Webpack DevServer
Webpack DevServer will reload pages as changes are made to watched items. Run **npm run serve** to launch the DevServer in the development configuration. This will rebuild the site upon any code changes and promote them to ./dev. This will also launch a browser to the WordPress site running in the above Docker containers. This Docker configuration allows for reloading of files, so changes updated by DevServer will be visible to the Docker instance immediately. Run **npm run debug** to build the site, stand it up in Docker, then attach the DevServer to the build directory. This will effectively launch your browser on a copy of the the latest code. Additionally, the DevServer will trigger a reload in your browser upon detected changes.
