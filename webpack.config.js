const path = require("path");
const TerserPlugin = require("terser-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");

const entry = {
	front: [
		"./sass/index.scss"
		,"./typescript/index.ts"
	]
};

const export_module = {
	rules: [{
		test: /\.ts$/
		,use: "ts-loader"
	}
	,{
		test: /\.s[ac]ss$/i
		,use: [
			"style-loader"
			,"css-loader"
			,"sass-loader"
		]
	}]
};

const resolve = {
	extensions: [".ts", ".js", ".scss"]
};

const output_filename = "js/[name].min.js";

const output_library = "zinelib";

const optimization = {
	minimize: true
	,minimizer: [new TerserPlugin({
		terserOptions: {
			mangle: false
		}
	})]
};

const plugins = [
	new CopyPlugin({
		patterns: [
			{	from: "ZineLibrarian.php"
				,to: ""
			},{	from: "COPYING"
				,to: ""
			},{	from: "templates"
				,to: "templates"
			},{	from: "repositories"
				,to: "repositories"
			},{	from: "services"
				,to: "services"
			},{	from: "node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"
				,to: "js/bootstrap.bundle.min.js"
			},{	from: "node_modules/bootstrap/dist/css/bootstrap.min.css"
				,to: "css/bootstrap.min.css"
			}
		]
	})
];

const dist = {
	entry: entry
	,name: "dist"
	,mode: "production"
	,module: export_module
	,resolve: resolve
	,output: {
		filename: output_filename
		,path: path.resolve(__dirname, "dist")
		,library: output_library
	}
	,optimization: optimization
	,plugins: plugins
};

const dev = {
	entry: entry
	,name: "dev"
	,mode: "development"
	,devtool: "source-map"
	,devServer: {
		static: ["dev"],
		devMiddleware: { writeToDisk: true },
		proxy: { "/": { target: "http://localhost:80/" } },
		open: true
	}
	,module: export_module
	,resolve: resolve
	,output: {
		filename: output_filename
		,path: path.resolve(__dirname, "dev")
		,library: output_library
	}
	,optimization: optimization
	,plugins: plugins
};

module.exports = [ dist, dev ];
