<?php
/*
 * DistroRepo - Repository for Distro objects
 * Copyright (C) 2021 The Bellingham Alternative Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
require_once(plugin_dir_path(__FILE__)."entities/Distro.php");

require_once(plugin_dir_path(__FILE__)."RepoBase.php");

if (!class_exists("DistroRepo")) {
    /**
     * Repository for Distro objects
     */
    class DistroRepo extends RepoBase
    {
        /**
         * Gets a distro by its unique ID
         * @param int $id the unique ID
         * @return Distro The distro requested
         */
        function GetById(int $id): ?Distro
        {
            return $this->map($this->wpdb->get_row($this->wpdb->prepare(
                "SELECT *
                 FROM zl_distro
                 WHERE
                    ID = '%d'",
                $id
            )));
        }

        /**
         * Gets a distro by its unique name
         * @param string $name the unique name
         * @return Distro The distro requested
         */
        function GetByName(string $name): ?Distro
        {
            return $this->map($this->wpdb->get_row($this->wpdb->prepare(
                "SELECT *
                 FROM zl_distro
                 WHERE
                    Name = '%s'",
                $name
            )));
        }

        /**
         * Gets a distro's ID by its unique name
         * @param string $name the unique name
         * @return string The distro's ID
         */
        function GetIdByName(string $name): ?string
        {
            return $this->wpdb->get_var($this->wpdb->prepare(
                "SELECT ID
                 FROM zl_distro
                 WHERE
                    Name = '%s'",
                $name
            ));
        }

        /**
         * Gets all distros in the table
         * @return array All distros
         */
        function GetAll(): ?array
        {
            return $this->wpdb->get_results(
                "SELECT *
                 FROM zl_distro"
            );
        }

        /**
         * Add a new distro to the table
         * @param Distro $distro the distro in question
         * @return string The new ID
         */
        function Add(Distro $distro): string
        {
            global $DistroFormat;

            $this->wpdb->insert(
                "zl_distro",
                (array) $distro,
                $DistroFormat
            );

            return $this->GetIdByName($distro->Name);
        }

        /**
         * Apply updates to a distro
         * @param Distro $distro the distro in question
         * @return string The ID of the distro
         */
        function Update(Distro $distro): string
        {
            $this->wpdb->update(
                "zl_distro",
                (array) $distro,
                array("ID" => $distro->ID),
                null,
                array("%d")
            );

            return $distro->ID;
        }

        /**
         * Delete a distro
         * @param int $id the distro ID
         */
        function DeleteById(int $id): void
        {
            $this->wpdb->delete(
                "zl_distro",
                array("ID" => $id),
                array("%d")
            );
        }

        /**
         * Maps a database result to a distro object
         * @param object $result the result from wpdb
         * @return Distro a distro entity
         */
        private function map(?object $result): ?Distro
        {
            if ($result == null)
                return $result;

            $distro = new Distro($result);

            return $distro;
        }
    }
}
