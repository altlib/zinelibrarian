<?php
/*
 * RepoBase - The base repository class
 * Copyright (C) 2021 The Bellingham Alternative Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
require_once(ABSPATH."wp-admin/includes/upgrade.php");

require_once(plugin_dir_path(__FILE__)."entities/Distro.php");
require_once(plugin_dir_path(__FILE__)."entities/Zine.php");

if (!class_exists("RepoBase")) {
    /**
     * The base repository class
     */
    class RepoBase {
        /**
         * @var wpdb $wpdb The WordPress database interface
         */
        protected $wpdb;

        /**
         * The base repository class
         */
        function __construct()
        {
            global $wpdb;

            $this->wpdb = $wpdb;
        }

        /**
         * Creates the database via WordPress dbDelta
         */
        function createDb(): void
        {
            global $DistroSchema;
            global $ZineSchema;

            dbDelta($DistroSchema);
            dbDelta($ZineSchema);
        }

        /**
         * Runs a conditional drop on tables if found
         * 
         * Note: This must be in reverse order due to FK relationships
         */
        function destroyDb(): void
        {
            global $wpdb;

            global $DistroDrop;
            global $ZineDrop;
            
            $wpdb->query($ZineDrop);
            $wpdb->query($DistroDrop);
        }
    }
}
