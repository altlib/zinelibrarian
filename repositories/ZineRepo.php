<?php
/*
 * ZineRepo - Repository for Zine objects
 * Copyright (C) 2021 The Bellingham Alternative Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
require_once(plugin_dir_path(__FILE__)."entities/Zine.php");

require_once(plugin_dir_path(__FILE__)."RepoBase.php");

if (!class_exists("ZineRepo")) {
    /**
     * Repository for Zine objects
     */
    class ZineRepo extends RepoBase
    {
        /**
         * Gets a zine by its unique ID
         * @param int $id the unique ID
         * @return Zine The zine requested
         */
        function GetById(int $id): ?Zine
        {
            return $this->map($this->wpdb->get_row($this->wpdb->prepare(
                "SELECT *
                 FROM zl_zine
                 WHERE
                    ID = '%d'",
                $id
            )));
        }

        /**
         * Gets a zine by its unique name
         * @param string $name the unique name
         * @return Zine The zine requested
         */
        function GetByName(string $name): ?Zine
        {
            return $this->map($this->wpdb->get_row($this->wpdb->prepare(
                "SELECT *
                 FROM zl_zine
                 WHERE
                    Name = '%s'",
                $name
            )));
        }

        /**
         * Gets a zine's ID by its unique name
         * @param string $name the unique name
         * @return string The zine's ID
         */
        function GetIdByName(string $name): ?string
        {
            return $this->wpdb->get_var($this->wpdb->prepare(
                "SELECT ID
                 FROM zl_zine
                 WHERE
                    Name = '%s'",
                $name
            ));
        }

        /**
         * Gets the number of zines associated with a given distro
         * @param int $distroId the distro ID to search by
         * @return int The number of associated zines
         */
        function GetCountByDistroId(int $distroId): int
        {
            return $this->wpdb->get_var($this->wpdb->prepare(
                "SELECT COUNT(ID)
                 FROM zl_zine
                 WHERE
                    DistroID = '%d'",
                $distroId
            ));
        }

        /**
         * Gets the number of zines associated with a given distro
         * @param string $distroId the distro ID to search by
         * @return int The number of associated zines
         */
        function GetCountByDistroName(string $distroName): int
        {
            return $this->wpdb->get_var($this->wpdb->prepare(
                "SELECT COUNT(ID)
                 FROM zl_zine z
                 INNER JOIN zl_distro d
                 ON (
                    z.DistroID = d.ID
                 )
                 WHERE
                    d.Name = '%s'",
                $distroName
            ));
        }

        /**
         * Gets an array of zines by DistroID
         * @param int $distroId the distro ID
         * @return array The zines requested
         */
        function GetByDistroId(int $distroId): array
        {
            $entities = array();

            $rows = $this->wpdb->get_results($this->wpdb->prepare(
                "SELECT *
                 FROM zl_zine
                 Where
                    DistroID = '%d'",
                $distroId
            ));

            foreach ($rows as $row) {
                $entity = new Zine();

                $entity = $this->map($row);

                array_push($entities, $entity);
            }

            return $entities;
        }

        /**
         * Gets all zines in the table
         * @return array All zines
         */
        function GetAll(): ?array
        {
            return $this->wpdb->get_results(
                "SELECT *
                 FROM zl_zine"
            );
        }

        /**
         * Add a new zine to the table
         * @param Zine $zine the zine in question
         * @return string The ID of the new zine
         */
        function Add(Zine $zine): string
        {
            global $ZineFormat;

            $this->wpdb->insert(
                "zl_zine",
                (array) $zine,
                $ZineFormat
            );

            return $this->GetIdByName($zine->Name);
        }

        /**
         * Apply updates to a zine
         * @param Zine $zine the zine in question
         * @return string The ID of the zine
         */
        function Update(Zine $zine): string
        {
            $this->wpdb->update(
                "zl_zine",
                (array) $zine,
                array("ID" => $zine->ID),
                null,
                array("%d")
            );

            return $zine->ID;
        }

        /**
         * Delete a zine
         * @param int $id the zine ID
         */
        function DeleteById(int $id): void
        {
            $this->wpdb->delete(
                "zl_zine",
                array("ID" => $id),
                array("%d")
            );
        }

        /**
         * Maps a database result to a zine object
         * @param object $result the result from wpdb
         * @return Zine a zine entity
         */
        private function map(?object $result): ?Zine
        {
            if ($result == null)
                return $result;

            $zine = new Zine($result);

            return $zine;
        }
    }
}
