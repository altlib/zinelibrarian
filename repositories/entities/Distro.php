<?php
/*
 * Distro - A zine distro
 * Copyright (C) 2021 The Bellingham Alternative Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
if (!class_exists("Distro")) {
    /**
     * @var string $DistroSchema Distro database table
     */
    global $DistroSchema;

    /**
     * @var string $DistroDrop Distro database table drop statement
     */
    global $DistroDrop;

    /**
     * @var string $DistroFormat Distro database table WordPress format
     */
    global $DistroFormat;

    /**
     * A zine distro
     */
    class Distro
    {
        /**
         * A zine distro
         */
        function __construct(?object $object = NULL)
        {
            if ($object) {
                $this->ID = $object->ID;
                $this->Name = $object->Name;
                $this->Bio = $object->Bio;
                $this->Link = $object->Link;
                $this->Image = $object->Image;
                $this->CreatedBy = $object->CreatedBy;
                $this->CreatedAt = $object->CreatedAt;
                $this->ModifiedBy = $object->ModifiedBy;
                $this->ModifiedAt = $object->ModifiedAt;
            }
        }

        public $ID;
        public $Name;
        public $Bio;
        public $Link;
        public $Image;
        public $CreatedBy;
        public $CreatedAt;
        public $ModifiedBy;
        public $ModifiedAt;
    }

    $DistroSchema =
        "CREATE TABLE zl_distro (
            ID          INT             NOT NULL    AUTO_INCREMENT,
            Name        VARCHAR(60)     NOT NULL    UNIQUE,
            Bio         VARCHAR(500)    NULL,
            Link        VARCHAR(2000)   NULL,
            Image       VARCHAR(2000)   NULL,
            CreatedBy   INT             NOT NULL,
            CreatedAt   DATETIME        NOT NULL,
            ModifiedBy  INT             NOT NULL,
            ModifiedAt  DATETIME        NOT NULL,
            PRIMARY KEY (ID)
        )";

    $DistroDrop = "DROP TABLE IF EXISTS zl_distro";

    $DistroFormat = array(
        "%d",
        "%s",
        "%s",
        "%s",
        "%s",
        "%d",
        "%s",
        "%d",
        "%s"
    );
}
