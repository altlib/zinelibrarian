<?php
/*
 * Zine - An individual zine
 * Copyright (C) 2021 The Bellingham Alternative Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
if (!class_exists("Zine")) {
    /**
     * @var string $ZineSchema Zine database table
     */
    global $ZineSchema;

    /**
     * @var string $ZineDrop Zine database table drop statement
     */
    global $ZineDrop;
    
    /**
     * @var string $ZineFormat Zine database table WordPress format
     */
    global $ZineFormat;

    /**
     * An individual zine
     */
    class Zine
    {
        /**
         * An individual zine
         */
        function __construct(?object $object = NULL)
        {
            if ($object) {
                $this->ID = $object->ID;
                $this->Name = $object->Name;
                $this->Description = $object->Description;
                $this->DistroID = $object->DistroID;
                $this->PubDate = $object->PubDate;
                $this->Link = $object->Link;
                $this->Image = $object->Image;
                $this->CreatedBy = $object->CreatedBy;
                $this->CreatedAt = $object->CreatedAt;
                $this->ModifiedBy = $object->ModifiedBy;
                $this->ModifiedAt = $object->ModifiedAt;
            }
        }

        public $ID;
        public $Name;
        public $Description;
        public $DistroID;
        public $PubDate;
        public $Link;
        public $Image;
        public $CreatedBy;
        public $CreatedAt;
        public $ModifiedBy;
        public $ModifiedAt;
    }

    $ZineSchema = 
        "CREATE TABLE zl_zine (
            ID          INT             NOT NULL    AUTO_INCREMENT,
            Name        VARCHAR(150)    NOT NULL    UNIQUE,
            Description VARCHAR(500)    NULL,
            DistroID    INT             NULL,
            PubDate     DATETIME        NULL,
            Link        VARCHAR(2000)   NULL,
            Image       VARCHAR(2000)   NULL,
            CreatedBy   INT             NOT NULL,
            CreatedAt   DATETIME        NOT NULL,
            ModifiedBy  INT             NOT NULL,
            ModifiedAt  DATETIME        NOT NULL,
            PRIMARY KEY (ID),
            FOREIGN KEY (DistroID)  REFERENCES  zl_distro(ID)
        )";

    $ZineDrop = "DROP TABLE IF EXISTS zl_zine";

    $ZineFormat = array(
        "%d",
        "%s",
        "%s",
        "%d",
        "%s",
        "%s",
        "%s",
        "%d",
        "%s",
        "%d",
        "%s"
    );
}
