<?php
/*
 * WordPressRepo - Repository for WordPress objects and properties
 * Copyright (C) 2021 The Bellingham Alternative Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
require_once(plugin_dir_path(__FILE__)."RepoBase.php");

if (!class_exists("WordPressRepo")) {
    /**
     * Repository for WordPress objects and properties
     */
    class WordPressRepo extends RepoBase
    {
        /**
         * Retrieve the URL of the first page found for a given template
         * @param string $templatePath The path to the template
         * @return string The URL of the first page found using this template
         */
        function GetFirstPageLinkByTemplate(string $templatePath): string
        {
            $templatePages = get_pages(array(
                "meta_key"      => "_wp_page_template",
                "meta_value"    => $templatePath
            ));

            if (sizeof($templatePages) > 0)
                return get_page_link($templatePages[0]->ID);
            else
                return "";
        }
    }
}
