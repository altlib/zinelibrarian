/*
 * managezines - TypeScript components for zine management
 * Copyright (C) 2021 The Bellingham Alternative Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * TypeScript components for zine management
 */
export class managezines
{
    /**
     * Handle the async submit form call
     * @param event The event triggering the submit
     */
    static zine_submit(event: Event): void
    {
        const validate_name: XMLHttpRequest = new XMLHttpRequest();
        const form: HTMLFormElement = document.getElementById("zine-form") as HTMLFormElement;
        const form_data: FormData = new FormData(form);

        validate_name.open(
            "GET",
            "admin-ajax.php?action=is_zine_by_name&name=" + form_data.get("name") as string,
            true
        );
        validate_name.onreadystatechange = () => {
            const submit_request: XMLHttpRequest = new XMLHttpRequest();

            if (validate_name.readyState === XMLHttpRequest.DONE && validate_name.status == 200) {
                if (form_data.get("id") as string != "-1" || validate_name.responseText !== "1") {
                    submit_request.open(
                        "POST",
                        form.getAttribute("action"),
                        true
                    );
                    submit_request.onreadystatechange = () => {
                        const modal_close_button: HTMLButtonElement = document.getElementById("zine-modal-close") as HTMLButtonElement;

                        if (submit_request.readyState === XMLHttpRequest.DONE && submit_request.status == 200) {
                            if (form_data.get("id") as string == "-1") {
                                this.add_to_table(submit_request.responseText, form_data.get("name") as string, form_data.get("desc") as string, form_data.get("pubdate") as string);
                            } else {
                                this.update_table(submit_request.responseText, form_data.get("name") as string, form_data.get("desc") as string, form_data.get("pubdate") as string);
                            }
                            modal_close_button.click();
                        }
                    };
                    submit_request.send(form_data);
                } else {
                    alert("A zine by the name " + form_data.get("name") as string + " already exists.");
                }
            }
        };
        validate_name.send();
    }

    /**
     * Pop up the modal for editing instead
     * @param event The edit button event
     */
    static edit_modal(event: MouseEvent): void
    {
        const button: HTMLAnchorElement = event.target as HTMLAnchorElement;
        const row: HTMLTableRowElement = button.parentElement.parentElement as HTMLTableRowElement;
        const id: string = row.id.replace("zine-row-", "");
        const zine_request: XMLHttpRequest = new XMLHttpRequest();
 
        zine_request.open(
            "GET",
            "admin-ajax.php?action=get_zine_by_id&id=" + id,
            true
        );
        zine_request.onreadystatechange = () => {
            const modal_toggle: HTMLButtonElement = document.getElementById("modal-toggle") as HTMLButtonElement;
            const modal_title: HTMLHeadingElement = document.getElementById("zine-modal-title") as HTMLHeadingElement;
            const modal_accept: HTMLButtonElement = document.getElementById("zine-modal-accept") as HTMLButtonElement;
            const id_input: HTMLInputElement = document.getElementById("idInput") as HTMLInputElement;
            const name_input: HTMLInputElement = document.getElementById("nameInput") as HTMLInputElement;
            const desc_input: HTMLTextAreaElement = document.getElementById("descInput") as HTMLTextAreaElement;
            const distro_input: HTMLInputElement = document.getElementById("distroInput") as HTMLInputElement;
            const pubdate_input: HTMLInputElement = document.getElementById("pubdateInput") as HTMLInputElement;
            const link_input: HTMLInputElement = document.getElementById("linkInput") as HTMLInputElement;
            const image_input: HTMLInputElement = document.getElementById("imageInput") as HTMLInputElement;

            let zine;
 
            if (zine_request.readyState === XMLHttpRequest.DONE && zine_request.status == 200) {
                modal_title.innerText = "Edit Zine";
                modal_accept.innerText = "Edit";
 
                zine = JSON.parse(zine_request.responseText);
                id_input.value = id;
                name_input.value = zine.Name;
                desc_input.value = zine.Description;
                distro_input.value = zine.DistroName;
                pubdate_input.value = zine.PubDate;
                link_input.value = zine.Link;
                image_input.value = zine.Image;
 
                modal_toggle.click();
            }
        };
        zine_request.send();
    }

    /**
     * Handle the async remove zine call
     * @param event The event triggering the delete
     */
    static delete_zine(event: MouseEvent): void
    {
        const request: XMLHttpRequest = new XMLHttpRequest();
        const button: HTMLAnchorElement = event.target as HTMLAnchorElement;
        const row: HTMLTableRowElement = button.parentElement.parentElement as HTMLTableRowElement;
        const id: string = row.id.replace("zine-row-", "");

        request.open(
            "POST",
            "admin-ajax.php?action=delete_zine&id=" + id,
            true
        );
        request.onreadystatechange = () => {
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status == 200) {
                    this.delete_from_table(id);
                }
            }
        };
        request.send();
    }

    /**
     * Reset the form on closure
     */
    static close_form(): void
    {
        const modal_title: HTMLHeadingElement = document.getElementById("zine-modal-title") as HTMLHeadingElement;
        const modal_accept: HTMLButtonElement = document.getElementById("zine-modal-accept") as HTMLButtonElement;
        const id_input: HTMLInputElement = document.getElementById("idInput") as HTMLInputElement;
        const form: HTMLFormElement = document.getElementById("zine-form") as HTMLFormElement;

        modal_title.innerText = "Add Zine";
        modal_accept.innerText = "Add";
        id_input.value = "-1";
     
        form.reset();
    }

    /**
     * Add a zine to the table asynchronously
     * @param id The ID of the zine
     * @param name The name of the zine
     * @param desc The description of the zine
     * @param pubdate The publication date of the zine
     */
    private static add_to_table(id: string, name: string, desc: string, pubdate: string): void
    {
        const table: HTMLTableElement = document.getElementById("zine-table") as HTMLTableElement;
        let tbody: HTMLTableSectionElement;
        const row: HTMLTableRowElement = document.createElement("tr") as HTMLTableRowElement;
        const id_cell: HTMLTableCellElement = document.createElement("th") as HTMLTableCellElement;
        const name_cell: HTMLTableCellElement = document.createElement("td") as HTMLTableCellElement;
        const desc_cell: HTMLTableCellElement = document.createElement("td") as HTMLTableCellElement;
        const pubdate_cell: HTMLTableCellElement = document.createElement("td") as HTMLTableCellElement;
        const edit_button: HTMLButtonElement = document.createElement("button") as HTMLButtonElement;
        const edit_cell: HTMLTableCellElement = document.createElement("td") as HTMLTableCellElement;
        const delete_button: HTMLAnchorElement = document.createElement("a") as HTMLAnchorElement;
        const delete_cell: HTMLTableCellElement = document.createElement("td") as HTMLTableCellElement;

        if (table.tBodies.length > 0) {
            tbody = table.tBodies[0];
        } else {
            tbody = table.createTBody();
        }

        id_cell.innerText = id;
        id_cell.scope = "row";
        row.appendChild(id_cell);

        name_cell.innerText = name;
        name_cell.id = "zine-name-" + id;
        row.appendChild(name_cell);

        desc_cell.innerText = desc;
        desc_cell.id = "zine-desc-" + id;
        row.appendChild(desc_cell);

        pubdate_cell.innerText = pubdate;
        pubdate_cell.id = "zine-pubdate-" + id;
        row.appendChild(pubdate_cell);

        edit_button.classList.add("btn", "btn-sm", "btn-primary");
        edit_button.onclick = (event: MouseEvent) => { this.edit_modal(event); };
        edit_button.innerText = "Edit";
        edit_cell.appendChild(edit_button);
        row.appendChild(edit_cell);

        delete_button.classList.add("btn", "btn-sm", "btn-danger");
        delete_button.onclick = (event: MouseEvent) => { this.delete_zine(event); };
        delete_button.innerText = "Delete";
        delete_cell.appendChild(delete_button);
        row.id = "zine-row-" + id;
        row.appendChild(delete_cell);
        tbody.appendChild(row);
    }

    /**
     * Update a zine row asynchronously
     * @param id The ID of the zine
     * @param name The name of the zine
     * @param desc The description of the zine
     * @param pubdate The publication date of the zine
     */
    private static update_table(id: string, name: string, desc: string, pubdate: string): void
    {
        const name_cell: HTMLTableCellElement = document.getElementById("zine-name-" + id) as HTMLTableCellElement;
        const desc_cell: HTMLTableCellElement = document.getElementById("zine-desc-" + id) as HTMLTableCellElement;
        const pubdate_cell: HTMLTableCellElement = document.getElementById("zine-pubdate-" + id) as HTMLTableCellElement;

        name_cell.innerText = name;
        desc_cell.innerText = desc;
        pubdate_cell.innerText = pubdate;
    }

    /**
     * Remove a zine from the table asynchronously
     * @param id The ID of the zine to remove
     */
    private static delete_from_table(id: string): void
    {
        const row: HTMLTableRowElement = document.getElementById("zine-row-" + id) as HTMLTableRowElement;
     
        row.remove();
    }
}
