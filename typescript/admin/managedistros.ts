/*
 * managedistros - TypeScript components for distro management
 * Copyright (C) 2021 The Bellingham Alternative Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * TypeScript components for distro management
 */
export class managedistros
{
    /**
     * Handle the async submit form call
     * @param event The event triggering the submit
     */
    static distro_submit(event: Event): void
    {
        const validate_name: XMLHttpRequest = new XMLHttpRequest();
        const form: HTMLFormElement = document.getElementById("distro-form") as HTMLFormElement;
        const form_data: FormData = new FormData(form);

        validate_name.open(
            "GET",
            "admin-ajax.php?action=is_distro_by_name&name=" + form_data.get("name") as string,
            true
        );
        validate_name.onreadystatechange = () => {
            const submit_request: XMLHttpRequest = new XMLHttpRequest();

            if (validate_name.readyState === XMLHttpRequest.DONE && validate_name.status == 200) {
                if (form_data.get("id") as string != "-1" || validate_name.responseText !== "1") {
                    submit_request.open(
                        "POST",
                        form.getAttribute("action"),
                        true
                    );
                    submit_request.onreadystatechange = () => {
                        const modal_close_button: HTMLButtonElement = document.getElementById("distro-modal-close") as HTMLButtonElement;
            
                        if (submit_request.readyState === XMLHttpRequest.DONE && submit_request.status == 200) {
                            if (form_data.get("id") as string == "-1") {
                                this.add_to_table(submit_request.responseText, form_data.get("name") as string, form_data.get("bio") as string);
                            } else {
                                this.update_table(submit_request.responseText, form_data.get("name") as string, form_data.get("bio") as string);
                            }
                            modal_close_button.click();
                        }
                    };
                    submit_request.send(form_data);
                } else {
                    alert("A distro by the name " + form_data.get("name") as string + " already exists.");
                }
            }
        };
        validate_name.send();   
    }

    /**
     * Pop up the modal for editing instead
     * @param event The edit button event
     */
    static edit_modal(event: MouseEvent): void
    {
        const button: HTMLAnchorElement = event.target as HTMLAnchorElement;
        const row: HTMLTableRowElement = button.parentElement.parentElement as HTMLTableRowElement;
        const id: string = row.id.replace("distro-row-", "");
        const distro_request: XMLHttpRequest = new XMLHttpRequest();

        distro_request.open(
            "GET",
            "admin-ajax.php?action=get_distro_by_id&id=" + id,
            true
        );
        distro_request.onreadystatechange = () => {
            let distro;
            const modal_toggle: HTMLButtonElement = document.getElementById("modal-toggle") as HTMLButtonElement;
            const modal_title: HTMLHeadingElement = document.getElementById("distro-modal-title") as HTMLHeadingElement;
            const modal_accept: HTMLButtonElement = document.getElementById("distro-modal-accept") as HTMLButtonElement;
            const id_input: HTMLInputElement = document.getElementById("idInput") as HTMLInputElement;
            const name_input: HTMLInputElement = document.getElementById("nameInput") as HTMLInputElement;
            const bio_input: HTMLTextAreaElement = document.getElementById("bioInput") as HTMLTextAreaElement;
            const link_input: HTMLInputElement = document.getElementById("linkInput") as HTMLInputElement;
            const image_input: HTMLInputElement = document.getElementById("imageInput") as HTMLInputElement;

            if (distro_request.readyState === XMLHttpRequest.DONE && distro_request.status == 200) {
                modal_title.innerText = "Edit Distro";
                modal_accept.innerText = "Edit";

                distro = JSON.parse(distro_request.responseText);
                id_input.value = id;
                name_input.value = distro.Name;
                bio_input.value = distro.Bio;
                link_input.value = distro.Link;
                image_input.value = distro.Image;

                modal_toggle.click();
            }
        };
        distro_request.send();
    }

    /**
     * Handle the async remove distro call
     * @param event The event triggering the delete
     */
    static delete_distro(event: MouseEvent): void
    {
        const count_request: XMLHttpRequest = new XMLHttpRequest();
        const button: HTMLAnchorElement = event.target as HTMLAnchorElement;
        const row: HTMLTableRowElement = button.parentElement.parentElement as HTMLTableRowElement;
        const id: string = row.id.replace("distro-row-", "");

        count_request.open(
            "GET",
            "admin-ajax.php?action=check_distroid_zine_count&id=" + id,
            true
        );
        count_request.onreadystatechange = () => {
            const delete_request: XMLHttpRequest = new XMLHttpRequest();

            if (
                count_request.readyState === XMLHttpRequest.DONE
                && count_request.status == 200
                && !isNaN(+count_request.responseText)
            ) {
                if (count_request.responseText === "0") {
                    delete_request.open(
                        "POST",
                        "admin-ajax.php?action=delete_distro&id=" + id,
                        true
                    );
                    delete_request.onreadystatechange = () => {
                        if (delete_request.readyState === XMLHttpRequest.DONE) {
                            if (delete_request.status == 200) {
                                this.delete_from_table(id);
                            }
                        }
                    };
                    delete_request.send();
                } else {
                    alert("Cannot delete this distro, it has " + count_request.responseText + " zine(s) in the catalog.");
                }
            }
        };
        count_request.send();
    }

    /**
     * Reset the form on closure
     */
    static close_form(): void
    {
        const modal_title: HTMLHeadingElement = document.getElementById("distro-modal-title") as HTMLHeadingElement;
        const modal_accept: HTMLButtonElement = document.getElementById("distro-modal-accept") as HTMLButtonElement;
        const id_input: HTMLInputElement = document.getElementById("idInput") as HTMLInputElement;
        const form: HTMLFormElement = document.getElementById("distro-form") as HTMLFormElement;

        modal_title.innerText = "Add Distro";
        modal_accept.innerText = "Add";
        id_input.value = "-1";

        form.reset();
    }

    /**
     * Add a distro to the table asynchronously
     * @param id The ID of the distro
     * @param name The name of the distro
     * @param bio The bio of the distro
     */
    private static add_to_table(id: string, name: string, bio: string): void
    {
        const table: HTMLTableElement = document.getElementById("distro-table") as HTMLTableElement;
        let tbody: HTMLTableSectionElement;
        const row: HTMLTableRowElement = document.createElement("tr") as HTMLTableRowElement;
        const id_cell: HTMLTableCellElement = document.createElement("th") as HTMLTableCellElement;
        const name_cell: HTMLTableCellElement = document.createElement("td") as HTMLTableCellElement;
        const bio_cell: HTMLTableCellElement = document.createElement("td") as HTMLTableCellElement;
        const edit_button: HTMLButtonElement = document.createElement("button") as HTMLButtonElement;
        const edit_cell: HTMLTableCellElement = document.createElement("td") as HTMLTableCellElement;
        const delete_button: HTMLAnchorElement = document.createElement("a") as HTMLAnchorElement;
        const delete_cell: HTMLTableCellElement = document.createElement("td") as HTMLTableCellElement;
 
        if (table.tBodies.length > 0)
            tbody = table.tBodies[0];
        else
            tbody = table.createTBody();
 
        id_cell.innerText = id;
        id_cell.scope = "row";
        row.appendChild(id_cell);
 
        name_cell.innerText = name;
        name_cell.id = "distro-name-" + id;
        row.appendChild(name_cell);
 
        bio_cell.innerText = bio;
        bio_cell.id = "distro-bio-" + id;
        row.appendChild(bio_cell);
 
        edit_button.classList.add("btn", "btn-sm", "btn-primary");
        edit_button.onclick = (event: MouseEvent) => { this.edit_modal(event); };
        edit_button.innerText = "Edit";
        edit_cell.appendChild(edit_button);
        row.appendChild(edit_cell);
 
        delete_button.classList.add("btn", "btn-sm", "btn-danger");
        delete_button.onclick = (event: MouseEvent) => { this.delete_distro(event); };
        delete_button.innerText = "Delete";
        delete_cell.appendChild(delete_button);
        row.id = "distro-row-" + id;
        row.appendChild(delete_cell);
        tbody.appendChild(row);
    }

    /**
     * Update a distro row asynchronously
     * @param id The ID of the distro
     * @param name The name of the distro
     * @param bio The bio of the distro
     */
    private static update_table(id: string, name: string, bio: string): void
    {
        const name_cell: HTMLTableCellElement = document.getElementById("distro-name-" + id) as HTMLTableCellElement;
        const bio_cell: HTMLTableCellElement = document.getElementById("distro-bio-" + id) as HTMLTableCellElement;

        name_cell.innerText = name;
        bio_cell.innerText = bio;
    }

    /**
     * Remove a distro from the table asynchronously
     * @param id The ID of the distro to remove
     */
    private static delete_from_table(id: string): void
    {
        const row: HTMLTableRowElement = document.getElementById("distro-row-" + id) as HTMLTableRowElement;

        row.remove();
    }
}
