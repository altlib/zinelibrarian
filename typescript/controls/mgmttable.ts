/*
 * mgmttable - Dynamic data management table
 * Copyright (C) 2021 The Bellingham Alternative Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * A simple data table generator using Bootstrap
 */
export class mgmttable
{
    /**
     * The ID of the table
     */
    private _table: HTMLTableElement;

    /**
     * Constructs a table with the given information
     * @param table_id The HTML ID of the table element to fill in
     * @param data The data to display in the table
     * @param fields An associative array of fields in data with their column labels
     * @param edit_callback An optional edit callback to fire on the edit button
     * @param delete_callback An optional delete callback to fire on the delete button
     */
    constructor(table_id: string, data: string, fields: string, edit_callback: (ev: MouseEvent) => void = null, delete_callback: (ev: MouseEvent) => void = null)
    {
        const data_array: Record<string, string>[] = JSON.parse(data);
        const field_object: Record<string, string> = JSON.parse(fields);

        this._table = document.getElementById(table_id) as HTMLTableElement;

        if (data_array && field_object) {
            const field_object_entries: [string, string][] = Object.entries(field_object);
            const table_head: HTMLTableSectionElement = this._table.createTHead();
            const table_body: HTMLTableSectionElement = this._table.createTBody();
            const edit_header: HTMLTableCellElement = document.createElement("th") as HTMLTableCellElement;
            const delete_header: HTMLTableCellElement = document.createElement("th") as HTMLTableCellElement;

            for (const [field, label] of field_object_entries) {
                const column_header: HTMLTableCellElement = document.createElement("th") as HTMLTableCellElement;
                    column_header.id = table_id + "-" + field;
                    column_header.scope = "col";
                    column_header.innerText = label;

                table_head.appendChild(column_header);
            }

            if (edit_callback) {
                edit_header.scope = "col";
                edit_header.innerText = "Edit";
                table_head.appendChild(edit_header);
            }

            if (delete_callback) {
                delete_header.scope = "col";
                delete_header.innerText = "Delete";
                table_head.appendChild(delete_header);
            }

            data_array.forEach((row) => {
                const table_row: HTMLTableRowElement = document.createElement("tr") as HTMLTableRowElement;
                const edit_cell: HTMLTableCellElement = document.createElement("td") as HTMLTableCellElement;
                const edit_button: HTMLButtonElement = document.createElement("button") as HTMLButtonElement;
                const delete_cell: HTMLTableCellElement = document.createElement("td") as HTMLTableCellElement;
                const delete_button: HTMLButtonElement = document.createElement("button") as HTMLButtonElement;

                for (const [field] of field_object_entries) {
                    const table_data: HTMLTableCellElement = document.createElement("td") as HTMLTableCellElement;
                        table_data.innerText = row[field];
                        table_data.headers = table_id + "-" + field;

                    table_row.appendChild(table_data);
                }

                if (edit_callback) {
                    edit_button.classList.add("btn", "btn-sm", "btn-primary");
                    edit_button.onclick = edit_callback;
                    edit_button.innerText = "Edit";
                    edit_cell.appendChild(edit_button);
                    table_row.appendChild(edit_cell);
                }

                if (delete_callback) {
                    delete_button.classList.add("btn", "btn-sm", "btn-danger");
                    delete_button.onclick = delete_callback;
                    delete_button.innerText = "Delete";
                    delete_cell.appendChild(delete_button);
                    table_row.appendChild(delete_cell);
                }

                table_body.appendChild(table_row);
            });
        }
    }
}
