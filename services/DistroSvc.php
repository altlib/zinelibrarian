<?php
/*
 * DistroSvc - A service for distro consumption and management
 * Copyright (C) 2021 The Bellingham Alternative Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
require_once(plugin_dir_path(__FILE__)."../repositories/WordPressRepo.php");

require_once(plugin_dir_path(__FILE__)."../repositories/DistroRepo.php");
require_once(plugin_dir_path(__FILE__)."../repositories/ZineRepo.php");

require_once(plugin_dir_path(__FILE__)."models/DistroModel.php");
require_once(plugin_dir_path(__FILE__)."models/DistroPostModel.php");

require_once(plugin_dir_path(__FILE__)."SvcBase.php");

if (!class_exists("DistroSvc")) {
    /**
     * A service for distro consumption and management
     */
    class DistroSvc extends SvcBase
    {
        /**
         * @var WordPressRepo $_wordPressRepo
         */
        private $_wordPressRepo;

        /**
         * @var DistroRepo $_distroRepo
         */
        private $_distroRepo;

        /**
         * @var ZineRepo $_zineRepo
         */
        private $_zineRepo;

        /**
         * A service for distro consumption and management
         */
        function __construct()
        {
            $this->_wordPressRepo = new WordPressRepo();

            $this->_distroRepo = new DistroRepo();
            $this->_zineRepo = new ZineRepo();
        }

        /**
         * Retrieve a single distro by ID
         * @param int $id the ID
         * @return DistroModel a distro
         */
        function get(int $id): DistroModel
        {
            $model = new DistroModel();

            $entity = $this->_distroRepo->GetById($id);

            if (isset($entity))
                $this->package($model, $entity);

            return $model;
        }

        /**
         * Retrieve the URL to the first Distro Info page found
         * @param int $id the ID
         * @return string the URL
         */
        function get_url(int $id): string
        {
            $pageLink = $this->_wordPressRepo->GetFirstPageLinkByTemplate("templates/DistroInfo.php");

            return $pageLink."?ID=".strval($id);
        }

        /**
         * Determine whether a given name is taken
         * @param string $name the name
         * @return bool whether it is taken
         */
        function exists_by_name(string $name): bool
        {
            $entity = $this->_distroRepo->GetByName($name);

            if ($entity)
                return true;
            else
                return false;
        }

        /**
         * Determine number of zines published by a distro by ID
         * @param int $id The distro ID
         * @return int Number of zines by this distro ID
         */
        function id_count(int $id): int
        {
            return $this->_zineRepo->GetCountByDistroId($id);
        }

        /**
         * Determine number of zines published by a distro by name
         * @param string $id The distro name
         * @return int Number of zines by this distro name
         */
        function name_count(string $name): int
        {
            return $this->_zineRepo->GetCountByDistroName($name);
        }

        /**
         * Get all the distros
         * @return array An array of distros
         */
        function read(): ?array
        { 
            return $this->_distroRepo->GetAll();
        }

        /**
         * Create a distro
         * @param DistroPostModel $postModel The model from the front-end
         * @return string The new ID
         */
        function create(DistroPostModel $postModel): string
        {
            $entity = new Distro();

            $this->map($entity, $postModel);
            $this->auditstamp($entity);

            return $this->_distroRepo->Add($entity);
        }

        /**
         * Update an existing distro
         * @param int $id The ID
         * @param DistroPostModel $postModel The model from the front-end
         * @return string The ID
         */
        function update(int $id, DistroPostModel $postModel): string
        { 
            $entity = $this->_distroRepo->GetById($id);

            $this->map($entity, $postModel);
            $this->auditstamp($entity);

            return $this->_distroRepo->Update($entity);
        }

        /**
         * Delete an existing distro
         * @param int $id The distro ID to delete
         */
        function delete(int $id): void
        {
            $this->_distroRepo->DeleteById($id);
        }

        /**
         * Map an entity from a post model
         * @param Distro $entity the database entity
         * @param DistroPostModel $postModel the post model
         */
        private function map(Distro $entity, DistroPostModel $postModel): void
        {
            $entity->Name = $postModel->Name;
            $entity->Bio = $postModel->Bio;
            $entity->Link = $postModel->Link;
            $entity->Image = $postModel->Image;
        }

        /**
         * Package an entity into a model
         * @param DistroModel $model the model to pack
         * @param Distro $entity the entity to pack into it
         */
        private function package(DistroModel $model, Distro $entity): void
        {
            $model->ID = $entity->ID;
            $model->Name = $entity->Name;
            $model->Bio = $entity->Bio;
            $model->Link = $entity->Link;
            $model->Image = $entity->Image;
        }
    }
}
