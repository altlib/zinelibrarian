<?php
/*
 * SvcBase - The base service class
 * Copyright (C) 2021 The Bellingham Alternative Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
if (!class_exists("SvcBase")) {
    /**
     * The base service class
     */
    class SvcBase
    {
        /**
         * @param multi $entity The entity being stamped
         */
        function auditstamp($entity): void
        {
            $user_id = get_current_user_id();
            $time = time();

            if (!$entity->CreatedBy)
                $entity->CreatedBy = $user_id;
            if (!$entity->CreatedAt)
                $entity->CreatedAt = date("Y-m-d H:i:s", $time);
            $entity->ModifiedBy = $user_id;
            $entity->ModifiedAt = date("Y-m-d H:i:s", $time);
        }
    }
}
