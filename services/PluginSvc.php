<?php
/*
 * PluginSvc - A service for plugin-related activities
 * Copyright (C) 2021 The Bellingham Alternative Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
require_once(plugin_dir_path(__FILE__)."../repositories/RepoBase.php");

require_once(plugin_dir_path(__FILE__)."SvcBase.php");

if (!class_exists("PluginSvc")) {
    /**
     * A service for plugin-related activities
     */
    class PluginSvc extends SvcBase
    {
        /**
         * @var RepoBase $_repoBase
         */
        private $_repoBase;

        /**
         * A service for plugin-related activities
         */
        function __construct()
        {
            $this->_repoBase = new RepoBase();
        }

        /**
         * Create the repository's database
         */
        function database_create(): void
        {
            $this->_repoBase->createDb();
        }

        /**
         * Destroy the repository's database
         */
        function database_destroy(): void
        {
            $this->_repoBase->destroyDb();
        }
    }
}
