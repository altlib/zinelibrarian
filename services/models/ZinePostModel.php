<?php
/*
 * ZinePostModel - Model format for writeable zine info
 * Copyright (C) 2021 The Bellingham Alternative Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

if (!class_exists("ZinePostModel")) {
    /**
     * Model format for writeable zine info
     */
    class ZinePostModel
    {
        /**
         * @var string $Name The name of the zine
         */
        public $Name;

        /**
         * @var string $Description A short description of the zine
         */
        public $Description;

        /**
         * @var string $DistroName The name of the distro
         */
        public $DistroName;

        /**
         * @var string $PubDate The date of publication
         */
        public $PubDate;

        /**
         * @var string $Link A URL, local or otherwise, to a downloadable copy
         */
        public $Link;

        /**
         * @var string $Image A URL to a thumbnail image or coverpage
         */
        public $Image;
    }
}
