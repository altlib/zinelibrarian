<?php
/*
 * DistroPostModel - Model format for writeable distro info
 * Copyright (C) 2021 The Bellingham Alternative Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

if (!class_exists("DistroPostModel")) {
    /**
     * Model format for writeable distro info
     */
    class DistroPostModel
    {
        /**
         * @var string $Name The name of the distro
         */
        public $Name;

        /**
         * @var string $Bio A short bio of the distro
         */
        public $Bio;

        /**
         * @var string $Link A URL to the distro's web presence
         */
        public $Link;

        /**
         * @var string $Image A URL to an image to represent the distro
         */
        public $Image;
    }
}
