<?php
/*
 * ZineModel - Model format for a zine
 * Copyright (C) 2021 The Bellingham Alternative Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
require_once(plugin_dir_path(__FILE__)."ZinePostModel.php");

if (!class_exists("ZineModel")) {
    /**
     * Model format for a zine
     */
    class ZineModel extends ZinePostModel
    {
        /**
         * @var int $ID The ID of the zine
         */
        public $ID;

        /**
         * @var int $DistroID The ID of the distro
         */
        public $DistroID;
    }
}
