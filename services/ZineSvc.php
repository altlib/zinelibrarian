<?php
/*
 * ZineSvc - A service for zine consumption and management
 * Copyright (C) 2021 The Bellingham Alternative Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
require_once(plugin_dir_path(__FILE__)."../repositories/WordPressRepo.php");

require_once(plugin_dir_path(__FILE__)."../repositories/DistroRepo.php");
require_once(plugin_dir_path(__FILE__)."../repositories/ZineRepo.php");

require_once(plugin_dir_path(__FILE__)."models/ZineModel.php");
require_once(plugin_dir_path(__FILE__)."models/ZinePostModel.php");

require_once(plugin_dir_path(__FILE__)."SvcBase.php");

if (!class_exists("ZineSvc")) {
    /**
     * A service for zine consumption and management
     */
    class ZineSvc extends SvcBase
    {
        /**
         * @var WordPressRepo $_wordPressRepo
         */
        private $_wordPressRepo;

        /**
         * @var DistroRepo $_distroRepo
         */
        private $_distroRepo;

        /**
         * @var ZineRepo $_zineRepo
         */
        private $_zineRepo;

        /**
         * A service for zine consumption and management
         */
        function __construct()
        {
            $this->_wordPressRepo = new WordPressRepo();

            $this->_distroRepo = new DistroRepo();
            $this->_zineRepo = new ZineRepo();
        }

        /**
         * Retrieve a single zine by ID
         * @param int $id the ID
         * @return ZineModel a zine
         */
        function get(int $id): ZineModel
        {
            $model = new ZineModel();

            $entity = $this->_zineRepo->GetById($id);
            if (isset($entity)) {
                $distro = $this->_distroRepo->GetById($entity->DistroID);

                $this->package($model, $entity);
                $model->DistroName = $distro->Name;
            }

            return $model;
        }

        /**
         * Retrieve an array of zines by distro
         * @param int $distro_id the distro ID
         * @return array An array of relevant zines
         */
        function get_by_distro_id(int $distro_id): array
        {
            $models = array();

            $entities = $this->_zineRepo->GetByDistroId($distro_id);
            if (sizeof($entities) > 0) {
                foreach ($entities as $entity) {
                    $model = new ZineModel();
                    $distro = $this->_distroRepo->GetById($entity->DistroID);

                    $this->package($model, $entity);
                    $model->DistroName = $distro->Name;

                    array_push($models, $model);
                }
            }

            return $models;
        }

        /**
         * Retrieve the URL to the first Zine Info page found
         * @param int $id the ID
         * @return string the URL
         */
        function get_url(int $id): string
        {
            $pageLink = $this->_wordPressRepo->GetFirstPageLinkByTemplate("templates/ZineInfo.php");

            return $pageLink."?ID=".strval($id);
        }

        /**
         * Determine whether a given name is taken
         * @param string $name the name
         * @return bool whether it is taken
         */
        function exists_by_name(string $name): bool
        {
            $entity = $this->_zineRepo->GetByName($name);

            if ($entity)
                return true;
            else
                return false;
        }

        /**
         * Get all the zines
         * @return array An array of zines
         */
        function read(): ?array
        {
            return $this->_zineRepo->GetAll();
        }

        /**
         * Create a zine
         * @param ZinePostModel $postModel The model from the front-end
         * @return string The ID of the newly created zine
         */
        function create(ZinePostModel $postModel): string
        {
            $distroID = $this->_distroRepo->GetIdByName($postModel->DistroName);
            $entity = new Zine();

            if ($distroID) {
                $entity->DistroID = $distroID;
            } else {
                $distro = new Distro();

                $distro->Name = $postModel->DistroName;
                $this->auditstamp($distro);

                $entity->DistroID = $this->_distroRepo->Add($distro);
            }

            $this->map($entity, $postModel);
            $this->auditstamp($entity);

            return $this->_zineRepo->Add($entity);
        }

        /**
         * Update an existing zine
         * @param int $id The ID
         * @param ZinePostModel $postModel The model from the front-end
         * @return string The ID
         */
        function update(int $id, ZinePostModel $postModel): string
        {
            $distroID = $this->_distroRepo->GetIdByName($postModel->DistroName);
            $entity = $this->_zineRepo->GetById($id);

            if ($distroID) {
                $entity->DistroID = $distroID;
            } else {
                $distro = new Distro();

                $distro->Name = $postModel->DistroName;
                $this->auditstamp($distro);

                $entity->DistroID = $this->_distroRepo->Add($distro);
            }

            $this->map($entity, $postModel);
            $this->auditstamp($entity);

            return $this->_zineRepo->Update($entity);
        }

        /**
         * Delete an existing zine
         * @param int $id The zine ID to delete
         */
        function delete(int $id): void
        { 
            $this->_zineRepo->DeleteById($id);
        }

        /**
         * Map an entity from a model
         * @param Zine $entity the database entity
         * @param ZinePostModel $postModel the post model
         */
        private function map(Zine $entity, ZinePostModel $postModel): void
        {
            $entity->Name = $postModel->Name;
            $entity->Description = $postModel->Description;
            $entity->PubDate = $postModel->PubDate;
            $entity->Link = $postModel->Link;
            $entity->Image = $postModel->Image;
        }

        /**
         * Package an entity into a model
         * @param ZineModel $model the model to pack
         * @param Zine $entity the entity to pack into it
         */
        private function package(ZineModel $model, Zine $entity): void
        {
            $model->ID = $entity->ID;
            $model->Name = $entity->Name;
            $model->Description = $entity->Description;
            $model->DistroID = $entity->DistroID;
            $model->PubDate = date('Y-m-d', strtotime($entity->PubDate));
            $model->Link = $entity->Link;
            $model->Image = $entity->Image;
        }
    }
}
