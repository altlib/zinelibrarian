<?php
/*
 * ManageDistros - A management screen for represented distros
 * Copyright (C) 2021 The Bellingham Alternative Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
require_once(plugin_dir_path(__FILE__)."../../services/DistroSvc.php");

$_distroSvc = new DistroSvc();

$distros = $_distroSvc->read();
?>
<div class="wrap">
    <div class="row">
        <h2>Distros<button id="modal-toggle" class="mx-2 btn btn-sm btn-success" type="button" data-toggle="modal" data-target="#distro-modal">Add</button></h2>
    </div>
    <div class="row">
        <table id="distro-table" class="table table-hover">
            <thead>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Bio</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
            </thead>
            <tbody>
<?php
                foreach ($distros as $distro) {
?>
                    <tr id="distro-row-<?php echo $distro->ID; ?>">
                        <th scope="row"><?php echo $distro->ID; ?></th>
                        <td id="distro-name-<?php echo $distro->ID; ?>"><?php echo $distro->Name; ?></td>
                        <td id="distro-bio-<?php echo $distro->ID; ?>"><?php echo $distro->Bio; ?></td>
                        <td><button class="btn btn-sm btn-primary" onclick="zinelib.managedistros.edit_modal(event);">Edit</button></td>
                        <td><button class="btn btn-sm btn-danger" onclick="zinelib.managedistros.delete_distro(event);">Delete</button></td>
                    </tr>
<?php
                }
?>
            </tbody>
        </table>
    </div>
</div>
<!-- modals -->
<div id="distro-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="distro-modal-title" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="distro-modal-title" class="modal-title">Add Distro</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close" onclick="zinelib.managedistros.close_form();">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="distro-form" method="post" action="<?php echo esc_html(admin_url("admin-ajax.php")); ?>" onsubmit="zinelib.managedistros.distro_submit(); return false;">
                    <input name="action" type="hidden" value="edit_distro"></input>
                    <input id="idInput" name="id" type="hidden" value="-1"></input>
                    <div class="form-group row">
                        <label for="nameInput" class="col-12 col-form-label">Name</label>
                        <div class="col-12">
                            <input id="nameInput" class="form-control" name="name" max=60 required></input>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="bioInput" class="col-12 col-form-label">Bio</label>
                        <div class="col-12">
                            <textarea id="bioInput" class="form-control" name="bio" type="textbox" max=500 rows=10></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="linkInput" class="col-12 col-form-label">Link</label>
                        <div class="col-12">
                            <input id="linkInput" class="form-control" name="link" type="url" max=2000></input>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="imageInput" class="col-12 col-form-label">Image</label>
                        <div class="input-group col-12">
                            <input id="imageInput" class="form-control" name="image" type="url" max=2000></input>
                            <div class="input-group-append">
                                <button type="button" class="btn btn-outline-secondary">Select</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="distro-modal-close" type="button" class="btn btn-secondary" data-dismiss="modal" onclick="zinelib.managedistros.close_form();">Close</button>
                <button id="distro-modal-accept" type="submit" class="btn btn-primary" form="distro-form">Add</button>
            </div>
        </div>
    </div>
</div>
