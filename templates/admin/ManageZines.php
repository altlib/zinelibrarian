<?php
/*
 * ManageZines - A management screen for the zine collection
 * Copyright (C) 2021 The Bellingham Alternative Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
require_once(plugin_dir_path(__FILE__)."../../services/DistroSvc.php");
require_once(plugin_dir_path(__FILE__)."../../services/ZineSvc.php");

$_distroSvc = new DistroSvc();
$_zineSvc = new ZineSvc();

$zines = $_zineSvc->read();
?>
<div class="wrap">
    <div class="row">
        <h2>Zines<button id="modal-toggle" class="mx-2 btn btn-sm btn-success" type="button" data-toggle="modal" data-target="#zine-modal">Add</button></h2>
    </div>
    <div class="row">
        <table id="zine-table" class="table table-hover">
            <thead>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">Publication Date</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
            </thead>
            <tbody>
<?php
                foreach ($zines as $zine) {
?>
                    <tr id="zine-row-<?php echo $zine->ID; ?>">
                        <th scope="row"><?php echo $zine->ID; ?></th>
                        <td id="zine-name-<?php echo $zine->ID; ?>"><?php echo $zine->Name; ?></td>
                        <td id="zine-desc-<?php echo $zine->ID; ?>"><?php echo $zine->Description; ?></td>
                        <td id="zine-pubdate-<?php echo $zine->ID; ?>"><?php echo date('Y-m-d', strtotime($zine->PubDate)); ?></td>
                        <td><button class="btn btn-sm btn-primary" onclick="zinelib.managezines.edit_modal(event);">Edit</button></td>
                        <td><button class="btn btn-sm btn-danger" onclick="zinelib.managezines.delete_zine(event);">Delete</button></td>
                    </tr>
<?php
                }
?>
            </tbody>
        </table>
    </div>
</div>
<!-- modals -->
<div id="zine-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="zine-modal-title" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="zine-modal-title" class="modal-title">Add Zine</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close" onclick="zinelib.managezines.close_form();">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="zine-form" method="post" action="<?php echo esc_html(admin_url("admin-ajax.php")); ?>" onsubmit="zinelib.managezines.zine_submit(); return false;">
                    <input name="action" type="hidden" value="edit_zine"></input>
                    <input id="idInput" name="id" type="hidden" value="-1"></input>
                    <div class="form-group row">
                        <label for="nameInput" class="col-12 col-form-label">Name</label>
                        <div class="col-12">
                            <input id="nameInput" class="form-control" name="name" max=150 required></input>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="descInput" class="col-12 col-form-label">Description</label>
                        <div class="col-12">
                            <textarea id="descInput" class="form-control" name="desc" type="textbox" max=500 rows=10></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="distroInput" class="col-12 col-form-label">Distro</label>
                        <div class="col-12">
                            <input id="distroInput" class="form-control" name="distro" max=60 required></input>
                            <!-- Add autocomplete that populates the names of known distros -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="pubdateInput" class="col-12 col-form-label">Publication Date</label>
                        <div class="col-12">
                            <input id="pubdateInput" class="form-control" name="pubdate" type="date"></input>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="linkInput" class="col-12 col-form-label">Link</label>
                        <div class="col-12">
                            <input id="linkInput" class="form-control" name="link" type="url" max=2000></input>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="imageInput" class="col-12 col-form-label">Image</label>
                        <div class="input-group col-12">
                            <input id="imageInput" class="form-control" name="image" type="url" max=2000></input>
                            <div class="input-group-append">
                                <button type="button" class="btn btn-outline-secondary">Select</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="zine-modal-close" class="btn btn-secondary" type="button" data-dismiss="modal" onclick="zinelib.managezines.close_form();">Close</button>
                <button id="zine-modal-accept" class="btn btn-primary" type="submit" form="zine-form">Add</button>
            </div>
        </div>
    </div>
</div>
