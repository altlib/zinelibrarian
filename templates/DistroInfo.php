<?php
/*
 * DistroInfo - An informational page about a distro
 * Copyright (C) 2021 The Bellingham Alternative Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
require_once(plugin_dir_path(__FILE__)."../services/DistroSvc.php");
require_once(plugin_dir_path(__FILE__)."../services/ZineSvc.php");

$_distroSvc = new DistroSvc();
$_zineSvc = new ZineSvc();

if (array_key_exists("ID", $_REQUEST))
    $id = $_REQUEST["ID"];
else
    $id = 0;

$distro = $_distroSvc->get($id);
$distro_zines = $_zineSvc->get_by_distro_id($id);

foreach ($distro_zines as $distro_zine) {
    $distro_zine->PageLink = $_zineSvc->get_url($distro_zine->ID);
}

get_header();
?>
<div class="mx-2 wrap">
    <div class="row">
        <div class="col-12 col-md-8">
            <div class="row mb-2">
                <div class="col-5">
                    <h2><?php echo $distro->Name; ?></h2>
                    <p><?php echo $distro->Bio; ?></p>
                    <p>Link: <a target="_blank" href="<?php echo $distro->Link; ?>"><?php echo $distro->Link; ?></a></p>
                </div>
                <div class="col-7">
                    <img class="img-fluid float-right" src="<?php echo $distro->Image; ?>" />
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-12">
                    <p class="mb-2">Zines by <?php echo $distro->Name; ?></p>
<?php
                    foreach ($distro_zines as $distro_zine) {
?>
                        <div class="row mb-1">
                            <div class="col-2">
                                <img class="img-fluid float-left" src="<?php echo $distro_zine->Image; ?>" />
                            </div>
                            <div class="col-10">
                                <h4><a href="<?php echo $distro_zine->PageLink; ?>"><?php echo $distro_zine->Name; ?></a></h4>
                                <p>Publication Date: <?php echo $distro_zine->PubDate; ?></p>
                                <p><?php echo $distro_zine->Description; ?></p>
                            </div>
                        </div>
<?php
                    }
?>
                </div>
            </div>
        </div>
        <div class="d-none d-md-block col-md-4">
            <!--Sidebar content-->
        </div>
    </div>
</div>

<?php get_footer();
